using NHibernate.AspNet.Identity.DomainModel;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace NHibernate.AspNet.Identity
{
    public class IdentityUserClaim : EntityWithTypedId<int>
    {
        public virtual string ClaimType { get; set; }

        public virtual string ClaimValue { get; set; }

        public virtual IdentityUser User { get; set; }
    }

    public class IdentityUserClaimMap : ClassMapping<IdentityUserClaim>
    {
        public IdentityUserClaimMap()
        {
            this.Table("AspNetUserClaims");
            this.Id(x => x.Id, m => m.Generator(Generators.Identity));
            this.Property(x => x.ClaimType);
            this.Property(x => x.ClaimValue);

            this.ManyToOne(x => x.User, m => m.Column("UserId"));
        }
    }

}
